;;; phrasal-verbs.el --- Search for phrasal verbs

;; Copyright 2021 cnngimenez
;;
;; Author: cnngimenez
;; Version: 0.1
;; Keywords: wp, covenience
;; URL: https://gitlab.com/cnngimenez/phrasal-verbs
;; Package-Requires: ((emacs "25.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'phrasal-verbs)

;;; Code:
(require 'seq)
(require 's)
(require 'cl-macs)
(require 'flycheck)
(require 'thingatpt)

(defgroup phrasal-verbs nil
  "Phrasal Verbs detection"
  :group 'convenience)

(defcustom phrasal-verbs-prepositions
  '("about" "for" "from" "in" "into" "of" "on" "to" "with")
  "List of prepositions used for recognising where is the phrasal verb."
  :type '(list string)
  :group 'phrasal-verbs)

(defcustom phrasal-verbs-dictionary-file "./dictionary.txt"
  "The dictionary with all the phrasal verbs.
The dictionary must be alphabetical sorted.  Use `sort-lines' if needed."
  :type 'file
  :group 'phrasal-verbs) ;; defcustom

;; Some portion of code were obtained from writegood-mode, like this face
;; definition.
(defface phrasal-verbs-preposition-face
  '((((supports :underline (:style wave)))
     :underline (:style wave :color "cyan"))
    (((class color))
     (:inherit font-lock-warning-face :background "LemonChiffon")))
  "A preposition face."
  :group 'phrasal-verbs)

;; -------------------------
;; Strutures

(cl-defstruct prep-pos-type
  "A Preposition string with their position data."
  preposition beginning end line line-beginning line-end)

(cl-defstruct phrasal-pos-type
  "A phrasal verb string with their position data."
  prep-pos
  phrasal)

;; -------------------------

(defun phrasal-verbs-find-all-preps-region (beg end)
  "Return the position of all preposition found at BEG and END region.
For each word in region, search it on `phrasal-verbs-prepositions' variable.

Return a list of prep-pos-type with the preposition found and its starting and
ending position."
  
  ;; ;; Find all prepositions strings
  ;; (seq-filter (lambda (word)
  ;;               (member word phrasal-verbs-prepositions))
  ;;             (s-split-words (buffer-substring-no-properties beg end)))

  (let ((lst-results nil))
    (save-excursion
      (goto-char beg)
      ;; Search until we reach the end point.
      (while (< (point) end)
        (let ((word (word-at-point)))
          (when (member word phrasal-verbs-prepositions)
            ;; It's in the list of prepositions... add it to the results.
            (push (make-prep-pos-type
                   :preposition word
                   :beginning (- (point) (length word))
                   :end (point)
                   :line (line-number-at-pos)
                   :line-beginning (- (- (point) (length word))
                                      (point-at-bol))
                   :line-end (- (point)
                                (point-at-bol)))
                  lst-results)))
        (forward-word)))
    lst-results) ) ;; defun


;; (defun phrasal-verbs-mark-all-preps ()
;;   "Mark all preposition with the appropiate face."
;;   (interactive)
;;   (seq-do (lambda (prep-pos)
;;             (add-face-text-property (car prep-pos)
;;                                     (nth 1 prep-pos)
;;                                     'phrasal-verbs-preposition-face t))
;;           (phrasal-verbs-find-all-preps-region (point-min) (point-max)))
;; ) ;; defun

(defun phrasal-verbs-preps-regexp ()
  "Generate the regex to find the prepositions."
  (concat "\\b" (regexp-opt phrasal-verbs-prepositions) "\\b")) ;; defun

(defun phrasal-verbs-preps-font-lock-keywords ()
  "Generate the font-lock keywords."
  (list (list (phrasal-verbs-preps-regexp)
              0 (quote 'phrasal-verbs-preposition-face) 'prepend)) ) ;; defun

(defun phrasal-verbs-preps-turn-on ()
  "Add preposition keywords to font-lock for syntax highlighting."
  (font-lock-add-keywords
   nil (phrasal-verbs-preps-font-lock-keywords) t) ) ;; defun

(defun phrasal-verbs-preps-turn-off ()
  "Remove the preposition keywords from font-lock."
  (font-lock-remove-keywords
   nil (phrasal-verbs-preps-font-lock-keywords)) ) ;; defun


(define-minor-mode phrasal-verbs-preps-mode
  "Mark all prepositions in the current buffer."
  :init-value nil
  :lighter "PvPreps"
  (if phrasal-verbs-preps-mode
      (phrasal-verbs-preps-turn-on)
    (phrasal-verbs-preps-turn-off))
  (font-lock-mode 1) )

(defun phrasal-verbs-open-dictionary ()
  "Open the dictionary in a buffer.
Open the file defined at `phrasal-verbs-dictionary-file' into a buffer."
  (find-file-noselect phrasal-verbs-dictionary-file) ) ;; defun

(defun phrasal-verbs-search-verb (verb)
  "Search for the given VERB in the text file, return the expressions found.
Returns a list of expresions found.

If VERB is nil, then return nil.

Open the dictionary and do a regexp search."
  (when verb
    (with-current-buffer (phrasal-verbs-open-dictionary)
      (goto-char (point-min))
      (let ((results nil))
        (while (search-forward-regexp (concat "^" verb "\\b.*$") nil t)
          (push (match-string-no-properties 0) results))
        results)))) ;; defun

;; TODO create an index from the dictionary with verbs only to search faster.

(defun phrasal-verbs-verb-at-end-sentence (sentence &optional threshold)
  "Search for the verb at the SENTENCE.
From the end of the sentence to the beginning, take each word and check if it
is in the dictionary.
If THRESHOLD is not given, then use 5 words as default.  This value is the
limit amount of words checked."
  (seq-find #'phrasal-verbs-search-verb
            (seq-take (reverse (s-split-words sentence))
                      (if threshold threshold 5))) ) ;; defun

(defun phrasal-verbs-prep-is-correct (preposition sentence)
  "Check if the preposition at the SENTENCE is correct.
The SENTENCE can be the string that is before the preposition.
The PREPOSITION is correct if the verb is found on the SENTENCE and the
verb + PREPOSITION is in the dictionary.
Return the verb founded if correct, nil otherwise."
  ;; Look for the verb at the end of the sentence.
  (let ((phrasal-verbs (phrasal-verbs-search-verb
                        (phrasal-verbs-verb-at-end-sentence sentence))))
    (when phrasal-verbs
      ;; found one... check if verb + preposition is in the phrasal verbs with
      ;; that verb.
      (seq-find (lambda (phrasal)
                  (string-match-p (concat preposition "\\b*$") phrasal))
                phrasal-verbs))) ) ;; defun

(defun phrasal-verbs-prep-pos-is-correct (prep-pos)
  "Check if the PREP-POS is \"correct\" under the current buffer.
PREP-POS is a list like '(beg-pos end-pos preposition-string) where beg-pos is
a position where the preposition begins, end-pos where it ends and
preposition-string the preposition itself.
It is \"correct\" if the verb founded at the sentence at beg-pos is found in
the phrasal verb dictionary.
Return a list with phrasal-verb-types. Return a phrasal-verb-type with nil
as its string if it is not correct."
  (let* ((sentence (buffer-substring-no-properties
                    (abs (- (prep-pos-type-beginning prep-pos) 100))
                    (prep-pos-type-end prep-pos)))
         (phrasal-verb-str (phrasal-verbs-prep-is-correct
                            (prep-pos-type-preposition prep-pos)
                            sentence)))
    
    (when phrasal-verb-str
      (make-phrasal-pos-type :phrasal phrasal-verb-str
                             :prep-pos prep-pos))) ) ;; defun

(defun phrasal-verbs-mark-phrasals-pos (phrasal-pos)
  "Mark the given PHRASAL-POS.
A PHRASAL-POS is a structure like '(phrasal-verb-string prep-pos) where prep-pos
is alist with '(beg-pos end-pos preposition-string)."
  (let ((beg (prep-pos-type-beginning (phrasal-pos-type-prep-pos phrasal-pos)))
        (end (prep-pos-type-end (phrasal-pos-type-prep-pos phrasal-pos))))
    (add-face-text-property beg end 'phrasal-verbs-preposition-face)
    (add-text-property beg end
                       '(font-lock-face phrasal-verbs-preposition-face))) )

(defun phrasal-verbs-wrong-phrasals ()
  "Return a list of \"wrong\" phrasal verbs and their position.
Return a list of phrasal-pos like the following: 
'((prep-pos phrasal-verb-string) ...)
Where prep-pos is '(beg-pos end-pos preposition-string)."
  ;; get a list of "wrong" prep-pos only:
  (remove nil
          ;; identify the correct phrasal verbs:
          (seq-map #'phrasal-verbs-prep-pos-is-correct
                   (phrasal-verbs-find-all-preps-region
                    (point-min) (point-max)))) ) ;; defun


(defun phrasal-verbs-mark-wrong-phrasals ()
  "Mark all wrong phrasal verbs.
Search the current buffer for wrong phrasal verbs.  \"Wrong\" means that the
phrasal verb is not in the dictionary or the preposition's verb was not found."
  (interactive)
  (seq-do #'phrasal-verbs-mark-phrasals-pos
          (phrasal-verbs-wrong-phrasals)) ) ;; defun

(defun phrasal-verbs-output-errors ()
  "Output errors to stdout for flycheck."
  (seq-do (lambda (phrasal-pos)
            (princ (format "%d-%d: Found at the dictionary: %s"
                           (prep-pos-type-beginning (phrasal-pos-type-prep-pos phrasal-pos))
                           (prep-pos-type-end (phrasal-pos-type-prep-pos phrasal-pos))
                           (phrasal-pos-type-phrasal phrasal-pos))))
          (phrasal-verbs-wrong-phrasals)) )

(flycheck-define-checker phrasal-verbs
  "Detect wrong phrasal verbs using phrasal-verbs.el"
  :command  ("emacs" (eval flycheck-emacs-args)
             "--eval" (eval (format "(progn (load-library \"%s\") (phrasal-verbs-output-errors))"
                                   (find-library-name "phrasal-verbs")))
             "--" source)
  :error-patterns ((error line-start (file-name) ":" line ":" column "-" end-column ":" (message)))
  :modes (org-mode fundamental-mode text-mode markdown-mode rst-mode) )

(provide 'phrasal-verbs)
;;; phrasal-verbs.el ends here
